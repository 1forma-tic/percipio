import {legend,start} from "./percipio.mjs";
legend.polarities=[
    "Fuite / action",
    "2",
    "Inibition/écoute/apprentissage",
    "4",
    "Aggression/affirmation",
    "6",
];
legend.concentricStep = [
        "⎫",
        "⎬ Bad",
        "⎭",
        "⎫",
        "⎬ Average",
        "⎭",
        "⎫",
        "⎬ Good",
        "⎭",
        "🠖 Ideal",
    ];
legend.mean="Mean";
legend.median="Median";
start();