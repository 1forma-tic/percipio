
export const legend = {
    concentricStep:[
        "⎫",
        "⎬ Bad",
        "⎭",
        "⎫",
        "⎬ Average",
        "⎭",
        "⎫",
        "⎬ Good",
        "⎭",
        "🠖 Ideal",
    ],
    mean:"Mean",
    median:"Median",
    polarities:[
        "Fuite/action",
        "2",
        "Inibition/écoute/apprentissage",
        "4",
        "Aggression/affirmation",
        "6",
    ]
}

import * as geom from "./percipioLib.mjs";
import * as glMatrix from '../node_modules/gl-matrix/esm/index.js';

export function start(){
    const circleR = 100;//98.5;
    const equiTriHeight=circleR*1.5;
    const equiTriSide=Math.round(1000*equiTriHeight*2/Math.sqrt(3))/1000;

    glMatrix.glMatrix.setMatrixArrayType(Array);
    const vec2 = glMatrix.vec2;
    const vec3 = glMatrix.vec3;
    const L = vec2.fromValues(-equiTriSide/2,circleR/2);
    const T = vec2.fromValues(0,-circleR);
    const R = vec2.fromValues(equiTriSide/2,circleR/2);
    geom.globals.vec3 = vec3;
    geom.globals.vec2 = vec2;
    geom.globals.L = L;
    geom.globals.T = T;
    geom.globals.R = R;

    const svgNode = document.querySelector('svg');

    let bgLayer = '' ;
    bgLayer += geom.svgPolygon([[0,0],[100,150],[100,0]], 'blue');
    bgLayer += geom.svgPolygon([[0,0],[100,0],[100,-150]], 'green');
    bgLayer += geom.svgPolygon([[0,0],[100,-150],[-100,-150]], 'yellow');
    bgLayer += geom.svgPolygon([[0,0],[-100,150],[-100,0]], 'red');
    bgLayer += geom.svgPolygon([[0,0],[-100,0],[-100,-150]], 'orange');
    bgLayer += geom.svgPolygon([[0,0],[-100,150],[100,150]], 'purple');

    bgLayer += geom.svgCircle([0,0], 150, 'none', 100, `white`);


    for (let i = 10; i>0;i--){
        bgLayer += geom.svgCircle([0,0], -5+i*10, 'none', 9, `hsla(0,0%,100%,${1-i*.1})`);
        bgLayer += geom.svgCircle([0,0], -5+i*10, 'none', 9, `hsla(0,0%,0%,${i*.04})`);
        bgLayer += geom.svgCircle([0,0], i*10, 'none', 1, `white`);
        bgLayer += geom.svgSquare([-117,-65-i*5], 5, `hsla(0,0%,0%,${i*.04})`, .8, 'white');
        bgLayer += geom.svgText([-112,-63-i*5],legend.concentricStep[10-i],"black","30%","begin")
    }
    bgLayer += geom.svgLine([-100,0], [100,0], 1 ,"white");
    bgLayer += geom.svgLine([-100,-150], [100,150], 1 ,"white");
    bgLayer += geom.svgLine([-100,150], [100,-150], 1 ,"white");

    bgLayer += geom.svgCircle([0,0], 10, `hsl(0,0%,${100-4}%)`, 1, 'white');

    bgLayer += geom.svgCircle([0,0], 10, `none`, .5, `hsl(100,70%,50%)`);
    bgLayer += geom.svgCircle([0,0], 40, `none`, .5, `hsl(50,70%,50%)`);
    bgLayer += geom.svgCircle([0,0], 70, `none`, .5, `hsl(0,70%,50%)`);
    bgLayer += geom.svgLine([-119,-72.5], [-115,-72.5], .4, `hsl(100,70%,50%)`);
    bgLayer += geom.svgLine([-119,-87.5], [-115,-87.5], .4, `hsl(50,70%,50%)`);
    bgLayer += geom.svgLine([-119,-102.5], [-115,-102.5], .4, `hsl(0,70%,50%)`);

    const equiTri=[
        [0,-circleR],
        [-equiTriSide/2,equiTriHeight-circleR],
        [equiTriSide/2,equiTriHeight-circleR],
    ];
    bgLayer += geom.svgPolygon(equiTri,"none",1,"black;stroke-linejoin:bevel")
    bgLayer += geom.svgCircle([0,0], 100, `none`, 1, `white`);

    bgLayer += geom.svgText([-.01,1,-.01],legend.polarities[0],"black","30%","middle")
    bgLayer += geom.svgText([-0.52,1,1],legend.polarities[1],"black","30%","begin")
    bgLayer += geom.svgText([-.01,-.01,1],legend.polarities[2],"black","30%","begin")
    bgLayer += geom.svgText([1,-0.55,1],legend.polarities[3],"black","30%","middle")
    bgLayer += geom.svgText([1,-.01,-.01],legend.polarities[4],"black","30%","end")
    bgLayer += geom.svgText([1,1,-0.52],legend.polarities[5],"black","30%","end")
//bgLayer += geom.svgText([1,.95,1],"Tendance 7","black","30%","middle")

    document.getElementById("backgroundLayer").innerHTML = bgLayer;

    document.getElementById("clickableArea").innerHTML
        = geom.svgCircle([0,0], 100, `transparent`)
        + geom.svgText([105,-100],"👁","black","150%","middle");
    document.getElementById("clickableArea").lastElementChild.addEventListener('click',()=>{
        const dots = Object.values(document.querySelectorAll(".fadeOff"));
        const points = dots.map(d=>{
            d.classList.remove("fadeOff");
            const p =[];
            p.push(parseFloat(d.getAttribute("cx")));
            p.push(parseFloat(d.getAttribute("cy")));
            return p;
        });
        const g = document.getElementById("interactiveShow");
        g.appendChild(geom.svgDomCircle(geom.mean(points),3,'hsl(240,80%,30%)'));
        g.appendChild(geom.svgDomCircle(geom.median(points),3,'hsl(30,80%,40%)'));

        g.appendChild(geom.svgDomCircle([-117,115],3,'hsl(240,80%,30%)'));
        g.appendChild(geom.svgDomText([-112,116.5],legend.mean,"black","30%","begin"));
        g.appendChild(geom.svgDomCircle([-117,107],3,'hsl(30,80%,40%)'));
        g.appendChild(geom.svgDomText([-112,108.5],legend.median,"black","30%","begin"));


    });

    document.getElementById("clickableArea").firstElementChild.addEventListener('click',(e)=>{
        const xy = geom.clickPos2svgXY(e,svgNode);
        const dot = geom.svgDomCircle(xy,2,"#333");
        document.getElementById("interactiveShow").appendChild(dot);
        setTimeout(()=>dot.classList.add('fadeOff'),100);
    });

    /*
            const testCase = {"score":[1,3,5]};
            import trikala from "../../generated/tmp/rv.trikala.mjs";
            const obj = {
                options:{
                    L: "1-0-0 Left",
                    T: "0-1-0 Top",
                    R: "0-0-1 Right",
                    toTopMedian: true,
                    toRightMedian: true,
                    debugSubAreaPosition: true,
                    showSubAreaName: true,
                    subArea: [
                        {
                            position: [0.2,1,0.2],
                            titlePosition: [0.2,1,0.2],
                            title: "Zone haute",
                            url: "http://perdu.com",
                            hue: 60,
                            description: "Bla bla bla en Markdown"
                        },
                        {position: [0.1,1,1], title: "Haut Droit", hue: 120, titlePosition: [-0.1,0.8,1]},
                        {position: [0.2,0.2,1], title: "Bas Droit", hue: 180},
                        {position: [1,0.1,1], title: "Bas", hue: 240},
                        {position: [1,0.2,0.2], title: "Bas Gauche", hue: 300},
                        {position: [1,1,0.1], title: "Haut Gauche", hue: 360, titlePosition: [1,0.8,-0.1]},
                        {position: [1,1,1], title: "Centre", titlePosition: [1,0.93,1]},
                    ]
                }
            };
            const wrappedJs = trikala(obj).split('\n');
            wrappedJs.shift(); wrappedJs.shift();
            wrappedJs.pop(); wrappedJs.pop();
            const toEval =`${wrappedJs.join('\n')} renderResult(${JSON.stringify(testCase)});`;
            eval(toEval);
    */
}
