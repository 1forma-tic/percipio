export const globals = {
    L:0,
    T:0,
    R:0,
    vec2:0,
    vec3:0,
}
export function bary2xy(array3){
    const v = globals.vec3.fromValues(...array3);
    const nv = globals.vec3.scale([],v, 1/(array3[0]+array3[1]+array3[2])); // normalize to 1
    const xy = globals.vec2.add([],
        globals.vec2.add([],
            globals.vec2.scale([], globals.L, nv[0]),
            globals.vec2.scale([], globals.T, nv[1])
        ),
        globals.vec2.scale([], globals.R, nv[2])
    );
    return xy;
}
export function optionalBary2xy(array3or2){
    return array3or2.length===2 ? array3or2 : bary2xy(array3or2);
}
export function svgCircle(centerPoint, size, fillColor, strokeSize=0, strokeColor='transparent') {
    const xy = optionalBary2xy(centerPoint);
    return `<circle cx="${xy[0]}" cy="${xy[1]}" r="${size}" style="fill:${fillColor};stroke-width:${strokeSize};stroke:${strokeColor};"/>`;
}
export function svgDomCircle(centerPoint, size, fillColor, strokeSize=0, strokeColor='transparent'){
    const xy = optionalBary2xy(centerPoint);
    const circle = document.createElementNS('http://www.w3.org/2000/svg','circle');
    circle.setAttribute(`cx`,`${xy[0]}`);
    circle.setAttribute(`cy`,`${xy[1]}`);
    circle.setAttribute(`r`,`${size}`);
    circle.style.fill = fillColor;
    circle.style.stroke = strokeColor;
    circle.style.strokeWidth = strokeSize;
    return circle;
}
export function svgLine(startPoint,endPoint, strokeSize=1, strokeColor='black') {
    const xy1 = optionalBary2xy(startPoint);
    const xy2 = optionalBary2xy(endPoint);
    return `<line x1="${xy1[0]}" y1="${xy1[1]}" x2="${xy2[0]}" y2="${xy2[1]}" style="stroke:${strokeColor};stroke-width: ${strokeSize};"/>`;
}
export function svgDomText(array3or2,text, color='black', size='12px', anchor="middle"){
    const xy = optionalBary2xy(array3or2);
    const svgText = document.createElementNS('http://www.w3.org/2000/svg','text');
    svgText.setAttribute(`x`,`${xy[0]}`);
    svgText.setAttribute(`y`,`${xy[1]}`);
    svgText.setAttribute(`text-anchor`,`${anchor}`);
    svgText.style.fill = color;
    svgText.style.fontSize = size;
    svgText.innerHTML = text;
    return svgText;
}
export function svgText(array3or2,text, color='black', size='12px', anchor="middle"){
    const xy = optionalBary2xy(array3or2);
    return `<text x="${xy[0]}" y="${xy[1]}" text-anchor="${anchor}" style="fill:${color};font-size:${size}">${text}</text>`;
}
export function svgPolygon(pointList, fillColor, strokeSize=0, strokeColor='transparent'){
    return `<polygon points="${pointList.map(p=>optionalBary2xy(p).join(',')).join(' ')}" style="fill:${fillColor};stroke-width:${strokeSize};stroke:${strokeColor};"/>`;
}
export function svgSquare(centerPoint, size, fillColor, strokeSize=0, strokeColor='transparent'){
    const xyCenter = optionalBary2xy(centerPoint);
    const pointList = [
        [xyCenter[0]-size/2,xyCenter[1]-size/2],
        [xyCenter[0]+size/2,xyCenter[1]-size/2],
        [xyCenter[0]+size/2,xyCenter[1]+size/2],
        [xyCenter[0]-size/2,xyCenter[1]+size/2],
    ];
    return `<polygon points="${pointList.map(p=>optionalBary2xy(p).join(',')).join(' ')}" style="fill:${fillColor};stroke-width:${strokeSize};stroke:${strokeColor};"/>`;
}
function svgRoundPath(pointList,fillColor){
    const midPoints = pointList.map((point,l)=>point.map((dimension,i)=>(dimension+pointList[(l+1)%pointList.length][i])/2));

    let path = '<path d="M '+optionalBary2xy(midPoints[midPoints.length-1]).join(',');
    for(let l in pointList){
        path+=' Q '+optionalBary2xy(pointList[l]).join(',')+' '+optionalBary2xy(midPoints[l]).join(',')
    }
    path+='" style="fill:'+fillColor+'"/>';
    return path;
}
export function clickPos2svgXY(event,svgNode){
    const exy = [event.clientX,event.clientY];
    const svgExt = svgNode.getBoundingClientRect();
    const svgInt = svgNode.viewBox.baseVal;
    const svgXY = [
        (exy[0]-svgExt.left)/svgExt.width * svgInt.width + svgInt.x,
        (exy[1]-svgExt.top)/svgExt.height * svgInt.height + svgInt.y,
    ];
    return svgXY;
}
export function mean(nDimPoints){
    const initialAccumulator = [];
    for (let dim =0;dim<nDimPoints[0].length;dim++)initialAccumulator.push(0);
    const nDimPointsSum=nDimPoints.reduce((acc,cur)=>cur.map((v,i)=>v+acc[i]),initialAccumulator);
    return nDimPointsSum.map(v=>v/nDimPoints.length);
}
function _stdDeviation(nDimPoints,filterFunc){
    const nDimPointsMean=mean(nDimPoints);
    const squareSum=nDimPoints.reduce((acc,cur)=>cur.map((v,i)=>Math.pow(filterFunc(0,v-nDimPointsMean[i]),2)+acc[i]),[0,0,0]);
    const variance=squareSum.map(v=>v/(nDimPoints.length-1));
    return variance.map(Math.sqrt);
}
function stdDeviation(nDimPoints){ // écart type
    return _stdDeviation(nDimPoints, (zero,v)=>v );
}

function underDeviation(nDimPoints){ // écart type inférieur
    return _stdDeviation(nDimPoints,Math.min);
}
function hoverDeviation(nDimPoints){ // écart type supérieur
    return _stdDeviation(nDimPoints,Math.max);
}

function splitPointsDimension(nDimPoints){
    const splitDim=[];
    nDimPoints[0].forEach(dim=>splitDim.push([]));
    nDimPoints.forEach(p=>p.forEach((dim,i)=>splitDim[i].push(dim)));
    return splitDim;
}
export function median(nDimPoints,ratio=.5){
    const splitDim=splitPointsDimension(nDimPoints);
    const sorted = splitDim.map(dim=>dim.sort((a, b) => a>=b ));
    const position = (nDimPoints.length-1)*ratio;
    const flooredP = Math.floor(position);
    const ratioP = position-flooredP;
    return sorted.map(d=> d[flooredP]*(1-ratioP) + d[Math.min(flooredP+1,nDimPoints.length-1)]*ratioP );
}
function area3Deviation(mid,dUnder,dHover){
    return [
        [mid[0]+dHover[0],mid[1],mid[2]],
        [mid[0],mid[1]-dUnder[1],mid[2]],
        [mid[0],mid[1],mid[2]+dHover[2]],
        [mid[0]-dUnder[0],mid[1],mid[2]],
        [mid[0],mid[1]+dHover[1],mid[2]],
        [mid[0],mid[1],mid[2]-dUnder[2]],
    ];
}
function area3quantile(mid,under,hover){
    return [
        [hover[0],mid[1],mid[2]],
        [mid[0],under[1],mid[2]],
        [mid[0],mid[1],hover[2]],
        [under[0],mid[1],mid[2]],
        [mid[0],hover[1],mid[2]],
        [mid[0],mid[1],under[2]],
    ];
}
